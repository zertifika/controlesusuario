﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ws = ControlesUsuario.wszfk;

namespace ControlesUsuario.Requests
{
    public class Requests
    {
        public string Request(string[] parametros)
        {
            string res = string.Empty;
            ws.Service1 wsZ = new ws.Service1();

            res = wsZ.DevolverFormulariosUsuario(parametros[0], parametros[1], parametros[2], parametros[3], parametros[4);

            return res;
        }
    }
}