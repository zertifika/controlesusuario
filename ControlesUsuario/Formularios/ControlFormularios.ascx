﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ControlFormularios.ascx.cs" Inherits="ControlesUsuario.ControlFormularios" %>

<div id="contenedor" class="container col-md-12">
    
    <div class="col-md-12">
           <h1 id="page-header" class="page-header col-md-12">GESTIÓN DE FORMULARIOS</h1>
    </div>
    <div class="col-md-12">
        <div class="row" id="montarBotones" style="text-align:left; padding-left:30px; padding-right:30px" runat="server">        
        
        </div>
    </div>
    <div class="col-md-12" style="height:20px">
        <div id="divTipoFormulario" class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <%--<select id="seleccionFormularios" class="col-lg-12 col-md-12 col-sm-12 col-xs-12" onchange="filtrarTiposFormulario()"></select>--%>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-4">
            <%-- ROW DIVPADREARBOL  --%>      
          <div class=""> 
            <div id="divpadrearbol" class="col-md-12 add-background add-shadow add-padding-top fixed-height" style="margin-top: 20px !important;">  
                <div class="row">
                   <div  id="jstree_demo_div_forms" style=" text-align:left; padding-left:20px"></div>
                    <div  id="jstree_demo_div_flujos" class="hidden" style=" text-align:left; padding-left:20px"></div>
                </div>          
                <div class="row" style="height:20px"> </div>
             </div>
          </div>
        </div>     
        <div class="col-md-8">
                
                <div id="contenido" class="add-background add-shadow fixed-height col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="float:left; font-size:12px; padding: 20px; margin-top: 20px !important;" >
                </div>
                <div id="mostrarFormulario" style="display:none">
                </div>
                <div id="trazasDocumento"></div>
                <div id="pag" style="display:none">0</div>

                <div style=" height:15px"></div>
               
       </div>
</div>
<div id="DivAuditoria" class="divInfo" style="display:none">    
   <div id='tablaauditoria'></div>
</div> 
<div id="NewFormulario" class="divInfo" style="display:none">    
    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 add-padding-top'>
        <p class='unificar text-left'>
            <label>Introduzca un nombre para el nuevo Expediente</label>
            <input id='txtNewForm' class="form-control" type='text'>
        </p>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sin-padding-right">
        <button onclick="CrearNuevoExpediente();" class="btn btn-AzulZertifika" style="float: right">Añadir</button>
            </div>
    </div>


</div>   
<div id="divexpInfo" class="divInfo" style="display:none;min-height:300px; margin-left:15px; margin-right:15px; margin-bottom:15px">    </div>   
<div id="Preview" class="divInfo" style="display:none" > </div>
<input type="hidden" id="Hiddenactualiza" />


<div id="divShareFormulario" style="display:none">
<form id="form1">
    <input type="hidden" id="FormId" value="" />
    <div id="PageContent" style="padding-bottom: 15px">
        <div class="rright" >
            <input type="hidden" id="Hidden1" />
            <input type="hidden" id="Hidden2" />
                    
            </div>
            <div id="Users" style="width:500px;" >
            <br />
                <div id='div1' >
                    <div style="font-family:initial; font-size:20px; padding-left:10px; width:290px"><input  type='radio' name='rcomp'  id='checkcomp1' value='Compartir con personas de su empresa' onclick='javascript:MostrarCuadroEmpresa()' checked="checked"/>Con gente de su Empresa </div>          
                    <div style="font-family:initial; font-size:20px; padding-left:10px; width:290px"><input type='radio' name='rcomp' id='checkcomp2' value='Compartir con un email' onclick='javascript:MostrarCuadroEmail()'/>Con un Email</div>
                </div>
               
                <div class="tablecompanies" style="width:445px">
                    
                        <input type="hidden" id="Hidden3" />
                        <input type="hidden" id="Hidden4" />
                        <input type="hidden" id="Hidden5" />
                </div>
            </div>
            <div>
               
            </div>
            <input type="button" class="btn btn-AzulZertifika"  style="margin-left: 100px;cursor:pointer;" onclick="javascript:ShareFormulario();" value="Comparta el Expediente">&nbsp;&nbsp;</input>
        </div>        
    </form>
 </div>
     <div id="divNuevoFormulario" style="display:none;max-height:665px">
              
        <div id="Campos" class="col-lg-4 col-md-4 col-sm-4 col-xs-12"  >
           
            <div id='divAmbitoFormulario' style="display:none">
                <div style="font-size:20px; padding-left:10px; width:290px"><input  type='radio' name='rAmbito'  id='rPropios' value='Tipo sólo accesible a usted' onclick='' checked="checked"/>Sólo para usted </div>          
                <div style="font-size:20px; padding-left:10px; width:290px"><input type='radio' name='rAmbito' id='rEmpresa' value='Tipo para toda su empresa' onclick=''/>Para toda la empresa</div>
            </div>
            <fieldset class="row sin-background"  >
                <p class="unificar text-left">
                    <label>Introduzca un nombre para el formulario</label>
                    <input type="text" id="txtNuevoFormulario" class="textEntry" />
                </p>
            </fieldset>
            <fieldset class="row sin-background"  >
                <p class="unificar text-left">
                    <label>Seleccione una carpeta</label>
                    <p id="NivelFormulario" >

                    </p>
                </p>
            </fieldset>
        </div>
        <div id='listaDocumentosFormulario' class="col-md-4" >
        </div>
        <div id="divsubida" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">    
            <form id="upload" enctype="multipart/form-data">
                <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="30000000000000000000000" />

                <div id="subidadocs">
	                <label for="fileselect">Selecciona ficheros a subir:</label>
	                <input type="file" id="fileselect" name="fileselect[]" multiple="multiple" />
	                <div id="filedrag">o suelte los ficheros aquí.</div>
                </div>
            </form>
            <div id="progress"></div>

            <div id="messages">

            </div>
        </div>
        
        <input type="button" class="btn btn-AzulZertifika"  style="float:right; cursor:pointer;" onclick="javascript: extraerCampos()" value="Subir Formulario">&nbsp;&nbsp;</input>        
     </div>

    <div id="divNuevoTipoFormulario" style="display:none;max-height:665px">
   
        <input type="hidden" id="IdTipo" value="" />                
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="Campos" >
           
            <div id='divAmbitoFormularioTipo' style="display:none" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div style="font-size:20px; padding-left:10px; width:290px"><input  type='radio' name='rAmbito'  id='rPropios' value='Tipo sólo accesible a usted' onclick='' checked="checked"/>Sólo para Usted </div>          
                <div style="font-size:20px; padding-left:10px; width:290px"><input type='radio' name='rAmbito' id='rEmpresa' value='Tipo para toda su empresa' onclick=''/>Para toda la Empresa</div>
            </div>
            <fieldset class=" sin-background col-lg-6 col-md-6 col-sm-6 col-xs-6"  >
                <p class="unificar text-left">
                    <label>Carpeta del Nuevo Tipo</label>
                    <p id="NivelTipoFormulario" >

                    </p>
                </p>
            </fieldset>
            <fieldset class=" sin-background col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <p class="unificar text-left">
                    <label>Introduzca el Nombre del Tipo</label>
                    <input type="text" id="txtNewTipo" class="textEntry" />
                </p>
            </fieldset>
            
        </div>
        
        <input type="button" class="btn btn-AzulZertifika"  style="float:right; cursor:pointer;" onclick="javascript: CrearTipoBD('Formularios');" value="Crear Tipo">&nbsp;&nbsp;</input>        
       
 </div>
    </div>