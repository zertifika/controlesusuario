﻿using System;
using System.Web;
using System.Web.Security;
using workFlow.wszfk;
using ControlesUsuario.ClassesControlador;
using System.Data;
using System.Web.Services;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using ws = workFlow.wszfk;
using System.Web.UI.WebControls;

namespace ControlesUsuario
{
    public partial class ControlFormularios : System.Web.UI.UserControl
    {
        public class Formulario
        {
            public string Nombre { get; set; }
            public int idFormulario { get; set; }
        }


        public class Etiqueta
        {
            public string Uniqueid { get; set; }
            public string Nombre { get; set; }
            public string Tipo { get; set; }
            public string Padre { get; set; }
            public string NombreFlujo { get; set; }
            public string Usuario { get; set; }
            public string fechaCreacion { get; set; }
            public int idFlujo { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            bool tienePermiso = false;
            string token = string.Empty, usuario = string.Empty, pass = string.Empty, idUsuario = string.Empty, empresa = string.Empty;
            
            if (url.Contains("signesPro") || url.Contains("signesDemos"))
            {
                // Autenticación de Signes.
                if ((!Utilities.validezUsuario(Request)) || (Request.Cookies["token"] == null))
                {
                    Response.Redirect("../cerrarSesion.aspx");
                    //FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    tienePermiso = true;
                    if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["token"].Values.ToString()))
                        idUsuario = HttpContext.Current.Request.Cookies["token"].Values.ToString().Split('I')[0].ToString();
                }
            }
            else
            {
                HttpCookie myCookie = new HttpCookie("Formulario");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                bool tieneToken = false;
                if (HttpContext.Current.Request.Cookies["token"] == null)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                {
                    idUsuario = HttpContext.Current.Request.Cookies["token"].Values.ToString().Split('I')[0].ToString();
                    tieneToken = true;
                    tienePermiso = true;
                }

                if (tieneToken && !this.Page.User.Identity.IsAuthenticated && Utilities.comprobarValidezUsuario(HttpContext.Current.Request.Cookies["token"].Value) < 0)
                {
                    FormsAuthentication.RedirectToLoginPage();
                }
                else
                    tienePermiso = true;
            }

            // Una vez que sabemos que el usuario tiene permisos extraemos los formularios
            if (tienePermiso)
            {
                //ws.Service1 wsZDocs = new ws.Service1();
                // En ZDocs se utiliza la clase Usuario donde se devuelve toda la info del mismo, pero para este caso no es necesario ya que con el 
                // idUsuario podemos recuperar la misma info
                string query = @"select email, password, usuarioEmpresa.idEmpresa from usuarios 
                                    inner join usuarioEmpresa on usuarioEmpresa.idUsuario = usuarios.idUsuario
                                    where usuarios.idUsuario = " + idUsuario;
                DataTable dtForm = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                if (dtForm.Rows.Count > 0)
                {
                    usuario = dtForm.Rows[0]["email"].ToString();
                    pass = dtForm.Rows[0]["password"].ToString();
                    empresa = dtForm.Rows[0]["idEmpresa"].ToString();
                }
                string forms = "Tabla de Formularios";

                //string[] parametros = new string[] { usuario, pass, "", empresa, "Formularios" };
                Service1 ws = new Service1();
                string res = ws.DevolverFormulariosUsuario(usuario, pass, "-1", empresa, "");
            }
            else
            {
                //arbolFormularios.Text = "Sin permisos";
            }
        }

        public static string GetFormulariosServer(string sEcho, string sSearch, int iDisplayLength, int iDisplayStart, int iColumns, int iSortingCols, string sColumns, int iSortCol, string sSortDir, string idTipoFormulario, bool esFlujo)
        {
            int tipoDoc = 1;
            int idUsuario = -1;
            try
            {
                tipoDoc = Convert.ToInt32(idTipoFormulario);
                var echo = int.Parse(sEcho);
                var displayLength = iDisplayLength;
                var displayStart = iDisplayStart;

                string tokenUsuario = HttpContext.Current.Request.Cookies["token"].Value;
                List<Formulario> records = GetRecordsFromDatabaseWithFilter(sSearch, tokenUsuario, iSortCol, sSortDir, iDisplayLength, iDisplayStart, esFlujo);
                if (records == null)
                    return string.Empty;

                var itemsToSkip = displayStart == 0 ? 0 : displayStart + 1;
                var pagedResults = records.Skip(itemsToSkip).Take(displayLength).ToList();
                var hasMoreRecords = false;

                var sb = new StringBuilder();
                sb.Append(@"{" + "\"recordsTotal\": " + records.Count + ",");
                sb.Append("\"recordsFiltered\": " + records.Count + ",");

                sb.Append("\"aaData\": [");

                foreach (var result in pagedResults)
                {
                    if (hasMoreRecords)
                        sb.Append(",");

                    sb.Append("[");
                    sb.Append("\"" + result.Nombre + "\",");
                    sb.Append("\"" + result.idFormulario + "\"");
                    sb.Append("]");
                    hasMoreRecords = true;
                }
                sb.Append("]}");

                return sb.ToString();
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("GetFlujosPendientes", "index2", idUsuario, e.Message, e.ToString());
                return null;
            }
        }
        
        public static List<Formulario> GetRecordsFromDatabaseWithFilter(string search, string tokenUsuario,int iSortCol, string sSortDir, int iDisplayLength, int iDisplayStart, bool esFlujo)
        {
            workFlow.wszfk.Service1 wsZ2 = new Service1();
            
            ws.Service1 wsZ = new ws.Service1();
            int idUsuario = -1;
            try
            {
                List<Formulario> formularios = new List<Formulario>();
                idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);

                List<SqlParameter> param = new List<SqlParameter>();
                param.Add(new SqlParameter("idUsuario", SqlDbType.Int));
                param[0].Value = idUsuario;
                // Debería ser una llamada al WS para devolver todos los formularios que toca e incluso por un tipo de formulario concreto
                string query = @"select email, password, usuarioEmpresa.idEmpresa from usuarios 
                                    inner join usuarioEmpresa on usuarioEmpresa.idUsuario = usuarios.idUsuario
                                    where usuarios.idUsuario = " + idUsuario;
                
                DataTable dtForm = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                string usuario = string.Empty, pass = string.Empty, empresa = string.Empty;
                if (dtForm.Rows.Count > 0)
                {
                    usuario = dtForm.Rows[0]["email"].ToString();
                    pass = dtForm.Rows[0]["password"].ToString();
                    empresa = dtForm.Rows[0]["idEmpresa"].ToString();
                }

                //string[] parametros = new string[] { usuario, pass, "", empresa, "Formularios" };
                Service1 ws = new Service1();
                string res = string.Empty;
                if (!esFlujo)
                {
                    res = ws.DevolverFormulariosUsuario(usuario, pass, "-1", empresa, "");
                    string[] forms = res.Split('|');
                    if (forms.Length > 0)
                    {                    
                        foreach (string f in forms)
                        {
                            if (!string.IsNullOrEmpty(f))
                            {
                                Formulario form = new Formulario();

                                form.idFormulario = Convert.ToInt32(f.Split('^')[0]);
                                form.Nombre = f.Split('^')[1];

                                formularios.Add(form);
                            }
                        }
                    }
                    // Si estamos buscando algo estará en search
                    if (!string.IsNullOrEmpty(search))
                    {
                        formularios = formularios.Where(a => a.Nombre.ToLower().Contains(search.ToLower())).ToList();
                    }
                }
                else
                {
                    query = @"select FormulariosFlujo.idFlujo, Flujo.Descripcion, Formularios.*  from formularios 
                            inner join formulariosFlujo on Formularios.idFormulario = FormulariosFlujo.idFormulario
                            inner join Flujo on flujo.idFlujo = FormulariosFlujo.idFlujo
                            inner join VisibilidadFlujo on VisibilidadFlujo.idFlujo = Flujo.idFlujo and VisibilidadFlujo.idUsuario = " +
                                idUsuario + " where Formularios.Activo = 1";

                    DataTable dtForms = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                    if (dtForms.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtForms.Rows.Count; i++)
                        {
                            Formulario form = new Formulario();

                            form.idFormulario = Convert.ToInt32(dtForms.Rows[i]["idFormulario"]);
                            form.Nombre = dtForms.Rows[i]["Nombre"].ToString();

                            formularios.Add(form);
                        }
                    }
                }
                // Por defecto ordeno por fecha inicio

                //formularios = formularios.OrderByDescending(a => a.fechaInicial).ToList();

                switch (iSortCol)
                {
                    case 0:

                        break;
                    case 1:

                        break;
                    case 2:
                        if (sSortDir == "asc")
                        {
                            formularios = formularios.OrderBy(a => a.Nombre).ToList();
                        }
                        else
                        {
                            formularios = formularios.OrderByDescending(a => a.idFormulario).ToList();
                        }
                        break;
                }

                return formularios;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("GetRecordsFromDatabaseWithFilter", "index2", idUsuario, e.Message, e.ToString());
                return null;
            }
        }

        public static string MostrarArbolFormularios(string divDestino, string tipoArbol)
        {
            string res = string.Empty;
            
            try
            {
                string token = HttpContext.Current.Request.Cookies["token"].Value;
                string idUsuario = token.Split('I')[0].ToString();
                string query = "select top 1 idEmpresa from UsuarioEmpresa where idUsuario = " + idUsuario;

                DataTable dtUsuario = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                if (dtUsuario.Rows.Count > 0)
                {
                    string idEmpresa = dtUsuario.Rows[0]["idempresa"].ToString();
                    List<Etiqueta> Lista1 = new List<Etiqueta> { };

                    Lista1 = DevolverListaTiposFormularios("", idUsuario.ToString(), idEmpresa, "-1", (tipoArbol.Equals("formularios") ? false : true));

                    string metodo = "javascript:cargarFormulariosServerControlador(\"" + tipoArbol + "\",\"" + divDestino + "\",\"-1\")";

                    if (tipoArbol.Equals("flujo"))
                        metodo = "javascript:cargarFormulariosServerControlador(\"" + tipoArbol + "\",\"" + divDestino + "\",\"-1\")";

                    res += "<ul><li data-jstree='{\"opened\":true,\"selected\":true}'><a onclick='" + metodo + "'>" + tipoArbol + "</a>";
                    // Si es de flujos solo habra un nivel más y estos no tendrán hijos
                    if (!tipoArbol.Equals("formularios")) res += "<ul>";
                    foreach (Etiqueta Nodo in Lista1)
                    {
                        res += CrearTablaCarpetasRecursivaFormularios(idUsuario.ToString(), idEmpresa, Nodo, "", "", 0, divDestino, tipoArbol);
                    }
                    if (!tipoArbol.Equals("formularios")) res += "</ul>";
                    res += "</li></ul>";
                        
                }
            }
            catch (Exception e)
            {
                string ex = e.ToString();
            }
            return res;
        }

        private static List<Etiqueta> DevolverListaTiposFormularios(string Tag, string idUsuario, string IdEmpresa, string Padre, bool esFlujo)
        {//ACTUALIZADO
            try
            {
                string query = "";
                if (esFlujo) // si es un flujo y -1 mostramos todos los flujos que el usuario tiene visibilidad
                        query = @"select flujo.* from Flujo
                                inner join VisibilidadFlujo on VisibilidadFlujo.idFlujo = Flujo.idFlujo 
                                    and VisibilidadFlujo.idUsuario = " + idUsuario + 
                              " where Flujo.Activo = 'S'";
                else
                    if (string.IsNullOrEmpty(Tag))
                        query = "select tip.idTipoFormulario,tip.descripcion,tip.Padre from TipoFormulario tip where (tip.idusuario='" + idUsuario + "' or idempresa='" + IdEmpresa + "') and Padre=" + Padre + " group by tip.idTipoFormulario,tip.descripcion,tip.Padre ";

                DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);

                List<Etiqueta> Hijos2 = new List<Etiqueta> { };

                if (datos.Rows.Count > 0)
                {
                    for (int i = 0; i < datos.Rows.Count; i++)
                    {
                        Etiqueta eti = new Etiqueta();
                        if (!esFlujo) 
                        {
                            eti.Uniqueid = datos.Rows[i]["idTipoFormulario"].ToString();
                            eti.Nombre = datos.Rows[i]["Descripcion"].ToString().Replace("\r\n", "");
                            eti.Padre = datos.Rows[i]["Padre"].ToString();
                        }
                        else
                        {
                            eti.idFlujo = Convert.ToInt32(datos.Rows[i]["idFlujo"].ToString());
                            eti.NombreFlujo = datos.Rows[i]["Descripcion"].ToString().Replace("\r\n", "");
                            eti.Usuario = datos.Rows[i]["idUsuario"].ToString();
                            eti.fechaCreacion = datos.Rows[i]["FechaCreacion"].ToString();
                        }
                        Hijos2.Add(eti);
                    }
                }
                return Hijos2;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static string CrearTablaCarpetasRecursivaFormularios(string IdUsuario, string IdFormulario, Etiqueta Nodo, string IdExpediente, string Size, int Zoom, string divDestino, string tipoArbol)
        {
            List<Etiqueta> Lista = new List<Etiqueta> { };
            string html = "";
            string metodo = "javascript:cargarFormulariosServerControlador(\"" + tipoArbol + "\",\"" + divDestino + "\",\"-1\")";

            if (tipoArbol.Equals("flujo"))
                metodo = "javascript:cargarFormulariosServerControlador(\"" + tipoArbol + "\",\"" + Nodo.Uniqueid + "\",\"-1\")";

            string nombreNodo = Nodo.Nombre;
            if (!tipoArbol.Equals("formularios"))
                nombreNodo = Nodo.NombreFlujo;
                

            html += "<li  ><a onclick='" + metodo + "'>" + nombreNodo + "</a>";
            if(tipoArbol.Equals("formularios"))
            {
                Lista = DevolverListaTiposFormularios("", IdUsuario, IdFormulario, Nodo.Uniqueid, false); // pasamos false pq sabemos cierto que es de tipo formulario

                if (Lista.Count > 0 && tipoArbol.Equals("formularios"))
                    html += "<ul>";
                foreach (Etiqueta Nodo2 in Lista)
                    html += CrearTablaCarpetasRecursivaFormularios(IdUsuario, IdFormulario, Nodo2, IdExpediente, Size, Convert.ToInt16(Zoom), divDestino, tipoArbol);
            }
            
            if (Lista.Count > 0 && tipoArbol.Equals("formularios"))
                html += "</ul>";
            html += "</li>";
            return html;
        }

        private static List<Etiqueta> devolverFlujosUsuario(string idUsuario, string idEmpresa)
        {
            List<Etiqueta> lista = new List<Etiqueta> { };
            try
            {
                string query = "select * from flujo where idUsuario = " + idUsuario;
                DataTable dtFlujo = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                for (int i = 0; i < dtFlujo.Rows.Count; i++)
                {
                    Etiqueta e = new Etiqueta();
                    e.NombreFlujo = dtFlujo.Rows[i]["Descripcion"].ToString();
                    e.idFlujo = Convert.ToInt32(dtFlujo.Rows[i]["idFlujo"].ToString());
                    lista.Add(e);
                }
                return lista;
            }
            catch (Exception e)
            {
                return lista;
            }
        }

    }
}