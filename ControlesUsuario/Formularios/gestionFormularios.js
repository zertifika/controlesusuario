﻿
function cargarAuditoria(IdDoc) {
    $.ajax({
        type: 'POST',
        url: "documentosHandler.ashx?op=Auditoria",
        data: { IdDoc: IdDoc },
        success: function (data) {
            $("#DivAuditoria").html("<br /> " + data);  //append
            $("#tablaAuditoria").dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
                },
                "columnDefs": [
                    { "targets": [0], "visible": false, "searchable": false }
                ],
                "order": [[0, "desc"]]
            });
            $("#tablaAuditoria_length").hide();
        }
    });
    $("#tablaAuditoria_length").hide();
}

function ComprobarAdmin() {

    $.ajax({
        type: "GET", url: "Funciones.svc/EsAdministrador",
        success: function (r) {

            if (r.d == true) {
                $("#divAmbitoFormulario").show();
                $("#divAmbitoFormularioTipo").show();
            } else {
                $("#divAmbitoFormulario").hide();
                $("#divAmbitoFormularioTipo").hide();

            }
        },

        error: function (req, textStatus, errorThrown) {
            if (textStatus == 'parsererror')
                $(location).attr('href', LOGIN_URL);
            else {
                jError("<div class='ico_error' />&nbsp " + errorThrown,
                {
                    HorizontalPosition: 'center',
                    VerticalPosition: 'top'
                });
            }
        }
    })
}


function CargarDocSinFormularios(idForm) {

    $.ajax({
        type: 'POST',
        url: "formulariosHandler.ashx?op=DocsFormularios",
        data: { IdFormulario: idForm },
        success: function (data) {
            $("#divDocSinExpediente").html(data);
            if ($("#tabledocsformulario")) $("#tabledocsformulario").dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
                },
                "columnDefs": [
                    { "targets": [0], "visible": false, "searchable": false }
//                    { "targets": [2], "searchable": false, "orderable": false },
//                    { "targets": [3], "searchable": false, "orderable": false },
//                    { "targets": [4], "searchable": false, "orderable": false },
//                    { "targets": [5], "searchable": false, "orderable": false }
//                    { "targets": [7], "searchable": false, "orderable": false }
                ],
                "order": [[0, "desc"]]
            });
            $("#tabledocsformulario_length").hide();
        }
    });
}

function cargarTodoFormulario() {
    $("#reformulariosNuevos").removeAttr("checked");
    $.ajax({
        type: 'POST',
        url: "formulariosHandler.ashx?op=cargarTodo",
        data: {},
        success: function (data) {
            $("#formularios").html(data);
            $("#formularios table").dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
                },
                "columnDefs": [
                    { "targets": [0], "searchable": true, "orderable": true },
                    { "targets": [1], "searchable": true, "orderable": true, "width": "300px" },
                    { "targets": [2], "searchable": true, "orderable": true, "width": "50px" },
                    { "targets": [3], "searchable": true, "orderable": true, "width": "50px" }
                ],
                "order": [[1, "desc"]]
            });
            $('tr:nth-child(odd)').addClass('odd');
            $("#formulariossNuevos_length").hide();
            //recordarPanel(true);
        }
    });
    $("#formulariosNuevos_length").hide();
    // Recargar("", "");
}


// Carga todos los formularios al hacer click encima del "tipo" raíz.
function cargarDocsFormulario(idForm, tag, leido) {
    // $("#rdocumentosNuevos").removeAttr("checked");
    $.ajax({
        type: 'POST',

        url: "FormulariosHandler.ashx?op=cargarTodo", //Funciones.svc/DocumentosEspediente",
        data: { IdFormulario: idForm, tag: tag, leido: leido },
        success: function (data) {
            //var tabla = $("#formularios table");
            //tabla.destroy();
            $("#formularios").html("");
            $("#formularios").html(data);
            $("#formularios table").dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
                },
                "order": [[1, "desc"]]
            });
            $('tr:nth-child(odd)').addClass('odd');
            $("#documentosFormulario_length").hide();
        }
    });
    $("#documentosFormulario_length").hide();
}

function cargarDocsFormularioTipo(idForm, tag, leido) {
    // $("#rdocumentosNuevos").removeAttr("checked");
    $.ajax({
        type: 'POST',
        url: "FormulariosHandler.ashx?op=cargarTodo", //Funciones.svc/DocumentosEspediente",
        data: { idFormulario: idForm, tag: tag, leido: leido },
        success: function (data) {
            $("#listaDocumentosFormulario").html(data);
            if ($("#tablaDocumentosFormulario"))
                $("#tablaDocumentosFormulario").dataTable({
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
                    },
                    "columnDefs": [
                        { "targets": [0], "visible": false, "searchable": false },
                        { "targets": [2], "searchable": false, "orderable": false },
                        { "targets": [3], "searchable": false, "orderable": false },
                        { "targets": [4], "searchable": false, "orderable": false },
                        { "targets": [5], "searchable": false, "orderable": false }
                    //{ "targets": [7], "searchable": false, "orderable": false }
                    ],
                    "order": [[1, "desc"]]
                });
            $('tr:nth-child(odd)').addClass('odd');
            $("#tablaDocumentosFormulario_length").hide();
        }
    });
    $("#tablaDocumentosFormulario_length").hide();
}


function cargarBusquedaDocsExpedientes(IdTipo, Tabla, Campos) {
    $.ajax({
        type: 'POST',
        url: "FormulariosHandler.ashx?op=cargarBusqueda",
        data: { Idtipo: IdTipo, Tabla: Tabla, Campos: Campos },
        success: function (data) {
            $("#documentos").html(data);
            if ($("#documentosExpediente")) $("#documentosExpediente").dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
                },
                "columnDefs": [
                    { "targets": [0], "visible": false, "searchable": false },
                    { "targets": [2], "searchable": false, "orderable": false },
                    { "targets": [3], "searchable": false, "orderable": false },
                    { "targets": [4], "searchable": false, "orderable": false },
                    { "targets": [5], "searchable": false, "orderable": false }


                ],
                "order": [[1, "desc"]]
            });
            $('tr:nth-child(odd)').addClass('odd');
            $("#documentosExpediente_length").hide();
        }
    });
    $("#documentosExpediente_length").hide();
}

function cargarNuevos(tag) {
    $("#rexpedientesTodos").removeAttr("checked");
    $.ajax({
        type: 'POST',
        url: "expedientesHandler.ashx?op=cargarNuevos",
        data: { tag: tag },
        success: function (data) {
            $("#expedientes").html(data);  //append
            if ($("#expedientesNuevos")) $("#expedientesNuevos").dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
                },
                "columnDefs": [
                    { "targets": [0], "visible": false, "searchable": false },
                    { "targets": [2], "searchable": false, "orderable": false },
                    { "targets": [3], "searchable": false, "orderable": false },
                    { "targets": [4], "searchable": false, "orderable": false },
                    { "targets": [5], "searchable": false, "orderable": false }
                    //{ "targets": [7], "searchable": false, "orderable": false }
                ],
                "order": [[0, "desc"]]
            });
            $("#expedientesNuevos_length").hide();
        }
    });
    $('tr:nth-child(odd)').addClass('odd');
    $("#expedientesNuevos_length").hide();
}

function cargaretiquetasFormulario(div, idForm) {
    $.ajax({
        type: "GET",
        url: "Funciones.svc/CrearTablaTiposNivelesDocsFormularios", //CrearTablaTipos", //CrearPrueba", //CrearTablaEtiquetas",
        data: {},
        dataType: "json",
        success: function (r) {
            $('#jstree_demo_div').html(r.d);
            $('#jstree_demo_div').jstree();
        }
    })

}

function mostrarDocFormulario(url, idDoc) {

    //$("#mostrarPdf").html("<iframe width='100%' height='600px' src='" + url + "'></iframe>");
    //$("#mostrarPdf").html("<iframe text-align='center' width='100%' height='100%' src='" + url + "'></iframe>");
    $("#mostrarPdf").html("<iframe  width='100%' height='100%' src='" + url + "'></iframe>");
    $.ajax({
        type: 'GET',
        //url: "documentosHandler.ashx?op=cargarTrazas&idDoc=" + idDoc,
        url: url,
        success: function (data) {
            //$("#trazasDocumento").html(data);
            //$("#mostrarPdf").append(Etiqueta);
            $("#mostrarPdf").dialog({
                autoOpen: true,
                resizable: true,
                closeOnEscape: true,
                height: 750,
                width: 750,
                position: { my: "left top", at: "left botom", of: $("#BtnNuevoDoc") },
                //position: { at: "left" },
                title: 'Preview del documento',
                modal: false,
                auto: false
            });


        }
    });
    addBotonCerrarDialogo();
}

//function CargarBusquedaDocsFormularios(IdTipo, Tabla, Campos) {
//    $("#rexpedientesNuevos").removeAttr("checked");
//    $.ajax({
//        type: 'POST',
//        url: "DocumentosExpediente.ashx?op=cargarBusqueda",
//        data: { Idtipo: IdTipo, Tabla: Tabla, Campos: Campos },
//        success: function (data) {
//            $("#expedientes").html(data);
//            $("#expedientesNuevos").dataTable({
//                "language": {
//                    "url": "//cdn.datatables.net/plug-ins/725b2a2115b/i18n/Spanish.json"
//                },
//                "columnDefs": [
//                    { "targets": [0], "visible": false, "searchable": false },
//                    { "targets": [2], "searchable": false, "orderable": false },
//                    { "targets": [3], "searchable": false, "orderable": false },
//                    { "targets": [4], "searchable": false, "orderable": false },
//                    { "targets": [5], "searchable": false, "orderable": false }
//                //{ "targets": [7], "searchable": false, "orderable": false }
//                ],
//                "order": [[0, "desc"]]
//            });
//            $("#expedientesNuevos_length").hide();
//        }
//    });
//    $("#expedientesNuevos_length").hide();
//}

function CrearFormulario() {
    $("#divsubida").css("display", "block");
    $("#filedrag").css("display", "block");
    $("#txtNuevoFormulario").text("");
    removeUsers = new Array();

    $("#divNuevoFormulario").dialog({
        autoOpen: true,
        resizable: true,
        closeOnEscape: true,
        height: 650,
        width: 1060,
        position: { my: "left top", at: "left botom", of: $("#page-header") },
        title: 'Nuevo Formulario',
        modal: true,
        auto: false,
        close: function () {
            $("#TbNewCampos").css("overflow-y", "none");
        }
    });
    addBotonCerrarDialogo();

    clicked = false;
    if (!arbolCargado) {
        arbolCargado = true;
        RellenarNivelesFormulario("NivelFormulario", false);
    }
    crearTablaDocumentosFormulario();
}

function redireccionarCrearFormularios() {
    setTimeout("location.href='CrearFormularios.aspx'", 0000);
}

function CrearTipoFormulario() {

    var ambito =

    $("#divNuevoTipoFormulario").dialog({
        autoOpen: true,
        resizable: true,
        closeOnEscape: true,
        height: 550,
        width: 800,
        position: { my: "left top", at: "left botom", of: $("#page-header") },
        title: 'Nuevo Tipo de Formulario',
        modal: true,
        auto: false,
        close: function () {
            $("#TbNewCampos").css("overflow-y", "none");
        }
    });
    addBotonCerrarDialogo();

    clicked = false;
    if (!arbolCargadoTipo) {
        arbolCargadoTipo = true;
        RellenarNivelesFormulario("NivelTipoFormulario", false);
    }
}
function RellenarNivelesFormulario(idDiv, mostrarMetadatos) {

    var json = "{\"idDiv\": \"" + idDiv + "\",\"mostrarMetadatos\": \"" + mostrarMetadatos + "\"}";

    $.ajax({
        type: "GET",
        url: "Funciones.svc/DevolverTiposParaArbolFormulario", //DevolverTiposParaSelect",
        data: JSON.parse(json),
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: function (r) {
            var html = ""; // "<select class='form-control' id='dropniveles'  >";
            html += r.d;

            //html += "</select>";
            $("#" + idDiv).html(html);
            $("#" + idDiv).jstree();
        },
        error: function (req, textStatus, errorThrown) {
            StopLoading();
            if (textStatus == 'parsererror')
                $(location).attr('href', LOGIN_URL);
            else {
                var err = JSON.parse(req.responseText);
                jError("<div class='ico_error' />&nbsp " + errorThrown,
                {
                    HorizontalPosition: 'center',
                    VerticalPosition: 'top'
                });
            }
        }
    });
}

function crearTablaDocumentosFormulario(idTipo, mostrarMetadatos) {
    $("#listaDocumentosFormulario ").html("");
    var tipo = idTipo;
    if (tipo == undefined || tipo == null || tipo == "") tipo = "-1";

    var json = "{\"idTipoFormulario\" : \"" + tipo + "\", \"mostrarMetadatos\": \"" + mostrarMetadatos + "\"}";

    $.ajax({
        type: "POST",
        url: "Formularios.aspx/devolverFormulariosTipo",
        data: json,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: function (r) {
            $("#listaDocumentosFormulario").html(r.d);
            $("#listaDocumentosFormulario > table").dataTable();
            $(".dataTables_filter").hide();
            $(".dataTables_length").hide();
        },
        error: function (req, textStatus, errorThrown) {
            if (textStatus == 'parsererror')
                $(location).attr('href', LOGIN_URL);
            else {
                var err = JSON.parse(req.responseText);
                jError("<div class='ico_error' />&nbsp " + errorThrown,
                {
                    HorizontalPosition: 'center',
                    VerticalPosition: 'top'
                });
            }
        }
    });
}

function abrirDialogoSubidaFormulario(idExp) {
    if (idExp != "" && idExp != undefined && idExp != "null")
        $("#divagregardocs").dialog("close");
    else
        $("#divsubida").dialog("close");

    $("#mostrarFormulario").html("<p class='text-left unificar'> <label>Previsualización Formulario.</label></p>");
    $("#mostrarCamposFormulario").html("<p class='text-left unificar'><label>Seleccione un formulario de la tabla para mostrar sus campos.</label></p>");
    $("#txtNuevoFormulario").text("");
    $("#divAgregarFormulario").dialog({
        autoOpen: true,
        resizable: true,
        closeOnEscape: true,
        height: 650,
        width: 1300,
        position: { my: "left top", at: "left botom", of: $("#page-header") },
        title: 'Nuevo Documento',
        modal: true,
        auto: false,
        close: function () {
            $("#TbNewCampos").css("overflow-y", "none");
        }
    });
    addBotonCerrarDialogo();

    if (!arbolCargadoNuevoDocFormulario) {
        arbolCargadoNuevoDocFormulario = true;
        RellenarNivelesFormulario("FormulariosDocumentosExpediente", true);
    }
    crearTablaDocumentosFormulario("-1", true);
    $("#botonAgregar").html("<input type='button' class='btn btn-AzulZertifika'  style='float:right; cursor:pointer;' onclick='javascript: subirDocumentoFormulario(" + idExp + ")' value='Añadir Documento'>&nbsp;&nbsp;</input>");
}

function mostrarMetadatosFormulario(idFormulario) {
    var json = "{\"idFormulario\" : \"" + idFormulario + "\"}"; //mostrarFormulario
    $.ajax({
        type: "POST",
        url: "Formularios.aspx/devolverMetadatosFormulario",
        data: json,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: function (r) {
            $("#mostrarFormulario").html(r.d.split("|\\//|")[0]);
            $("#mostrarCamposFormulario").html(r.d.split("|\\//|")[1]);
            var h = $("#mostrarCamposFormulario fieldset").height();
            $("#mostrarCamposFormulario").css("overflow-y", "scroll !important");
        },
        error: function (req, textStatus, errorThrown) {
            if (textStatus == 'parsererror')
                $(location).attr('href', LOGIN_URL);
            else {
                var err = JSON.parse(req.responseText);
                jError("<div class='ico_error' />&nbsp " + errorThrown,
                {
                    HorizontalPosition: 'center',
                    VerticalPosition: 'top'
                });
            }
        }
    });

}
function añadirInfoFormulario(url, idFormulario) { //sin-padding sin-margin

    $.ajax({
        type: "POST",
        url: "Formularios.aspx/devolverInfoDocVisor?idDocumento=" + idDoc,
        success: function (r) {

        }
    });
}

function mostrarFormulario(url) {
    var json = "{\"idFormulario\" : \"" + url.split("=")[1] + "\"}";

    $.ajax({
        type: "POST",
        url: "Formularios.aspx/devolverAccionesFormularios",
        data: json,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: function (data) {
            $("#mostrarFormulario").html("<div class='col-md-12'>" + data.d + "</div><div class='col-md-12' style='height:550px !important'><iframe width='100%' height='100%' src='" + url + "#zoom=page-width'></iframe></div>");
            $("#mostrarFormulario").dialog({
                autoOpen: true,
                resizable: true,
                canMaximize: true,
                closeOnEscape: true,
                height: 800,
                width: 800,
                //position: { my: "left top", at: "left botom+15", of: $("#page-header") },
                position: { my: "center center", at: "top center+20", of: document },
                title: 'Visualizador de Formulario',
                modal: false,
                auto: false,
                open: function (ev, ui) { addBotonCerrarDialogo(); },
                close: function (ev, ui) {
                }
            });
        }
    });
}

function subirDocumentoFormulario(idExp) {

    var idForm = $("#mostrarCamposFormulario span").text();
    var nomFormulario = $("#txtNuevoFormulario").val();
    var metadatos = "\"";
    $('#mostrarCamposFormulario p').each(function (e) {
        metadatos += $($(this).find("label")[0]).text() + "^" + $($(this).find("input")[0]).val() + "|";
    });
    metadatos = metadatos.substring(0, metadatos.length - 1) + "\"";

    var json = "{\"campos\" : " + metadatos.toString() + ",\"idFormulario\" : \"" + idForm + "\",\"idExpediente\" : \"" + idExp + "\",\"nombreFormulario\" : \"" + nomFormulario + "\"}";

    $.ajax({
        type: "POST",
        url: "Formularios.aspx/crearDocumentoFormulario",
        data: json,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: function (data) {
            $("#divAgregarFormulario").dialog("close");
        },
        error: function (req, textStatus, errorThrown) {
            if (textStatus == 'parsererror')
                $(location).attr('href', LOGIN_URL);
            else {
                var err = JSON.parse(req.responseText);
                jError("<div class='ico_error' />&nbsp " + errorThrown,
                {
                    HorizontalPosition: 'center',
                    VerticalPosition: 'top'
                });
            }
        }
    });
}

function modificarCamposDocumentoFormulario(idDoc, idFormulario, nombreDocumento, idExp) {
    var json = "{\"idDocumento\" : \"" + idDoc + "\",\"idFormulario\" : \"" + idFormulario + "\"}";
    if (idExp == "undefined") idExp = "";
    $.ajax({
        type: "POST",
        url: "Formularios.aspx/devolverMetadatosDocumentoFormulario",
        data: json,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: function (r) {
            $("#divModificacionCampos").html(r.d);
            //<input id="BtnNuevoDoc" class="btn btn-AzulZertifika" style="cursor: pointer; height: 30px; padding-top: 5px;" onclick="javascript:Init();AbrirDialogoSubida();" value="Nuevo Documento" type="button">
            $("#divModificacionCampos").append("<input id='btnModificarCampos' class='btn btn-AzulZertifika " +
                "'onclick='javascript:guardarCambiosDocumentoFormulario(" + idDoc + "," + idFormulario + ",\"" + nombreDocumento + "\",\"" + idExp + "\");' value='Guardar Documento' type='button' >");
            abrirDialogoModificarCampos(nombreDocumento);


        },
        error: function (req, textStatus, errorThrown) {
            if (textStatus == 'parsererror')
                $(location).attr('href', LOGIN_URL);
            else {
                var err = JSON.parse(req.responseText);
                jError("<div class='ico_error' />&nbsp " + errorThrown,
                {
                    HorizontalPosition: 'center',
                    VerticalPosition: 'top'
                });
            }
        }
    });
}

function abrirDialogoModificarCampos(nombreFormulario) {
    // campos del documento nombreFormulario
    $("#divModificacionCampos").dialog({
        autoOpen: true,
        resizable: true,
        canMaximize: true,
        closeOnEscape: true,
        height: 650,
        width: 500,
        //position: { my: "left top", at: "left botom+15", of: $("#page-header") },
        position: { my: "center center", at: "top center+20", of: document },
        title: 'Documento: ' + nombreFormulario,
        modal: false,
        auto: false,
        open: function (ev, ui) { addBotonCerrarDialogo(); },
        close: function (ev, ui) {
        }
    });
}


function guardarCambiosDocumentoFormulario(idDoc, idFormulario, nombreDocumento, idExp) {

    var metadatos = "\"";
    $('#divModificacionCampos p').each(function (e) {
        metadatos += $($(this).find("label")[0]).text() + "^" + $($(this).find("input")[0]).val() + "|";
    });
    metadatos = metadatos.substring(0, metadatos.length - 1) + "\"";

    var json = "{\"campos\" : " + metadatos.toString() + ",\"idFormulario\" : \"" + idFormulario + "\",\"idDocumentoActual\" : \"" + idDoc + "\",\"nombreDocumento\" : \"" + nombreDocumento + "\",\"idExpediente\" : \"" + idExp + "\"}";
    $.ajax({
        type: "POST",
        url: "Formularios.aspx/cambiarCamposDocumentoFormulario",
        data: json,
        contentType: "application/json; charset=UTF-8",
        dataType: "json",
        success: function (r) {
            $("#divModificacionCampos").dialog("close");
            $("#divModificacionCampos").html("");
            if (contains(window.location.href, "documentosNuevos"))
                Recargar();
            else
                cargarDocsExpediente(idexp, "", 'true');

        },
        error: function (req, textStatus, errorThrown) {
            if (textStatus == 'parsererror')
                $(location).attr('href', LOGIN_URL);
            else {
                var err = JSON.parse(req.responseText);
                jError("<div class='ico_error' />&nbsp " + errorThrown,
                {
                    HorizontalPosition: 'center',
                    VerticalPosition: 'top'
                });
            }
        }
    });
}
function contains(str, subStr) {
    if (str.indexOf(subStr) != -1) return true;
    else return false;
}


function mostrarPanelCompartirFormularios(estructura, titulo, idForm, idExp) {

    añadirHeaderCompartirFormularios(estructura, titulo, idForm, idExp);
    añadirConQuienCompartirYObligadaLecturaFormularios();
    añadirRellenablesFormularios();
    añadirListaCompartidosFormularios();

}

function añadirHeaderCompartirFormularios(estructura, titulo, IdDocumento, IdExpediente) {
    estructura = estructura.toUpperCase();
    var html = "<div id='page-header' class='col-lg-11 col-md-11 col-sm-11 col-xs-11' style='height: 30px'>" +
                   "<p id='compartirHeader' Doc='" + IdDocumento + "' Exp='" + IdExpediente + "' class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style='font-size: 20px'>" + estructura + " - " + titulo + "</p>" +
                "</div>" +
                "<div id='cerrar-header' class='col-lg-1 col-md-1 col-sm-1 col-xs-1' style='height: 30px'>" +
                   "<i class='fa fa-times fa-fx' onclick='cerrarPanel()'></i>" +
                "</div>";

    $("#panelOpciones").append(html);
}

function añadirConQuienCompartirYObligadaLecturaFormularios() {
    var html = "<div id='conQuienChecboxes' class='sin-padding-left col-lg-3 col-md-3 col-sm-4 col-xs-12 sin-padding-right'>" +
                    "<div id='conQuien' class='reducirAlturaMitad sin-padding-right col-lg-12 col-md-12 col-sm-12 col-xs-12'>" +
                    "<label style='color:#333;padding-left: 15px; margin-bottom: 0px !important'>Cómo compartir: </label>" +
                        "<div class='alinearConQuien col-md-12'>" +
                            "<input class='col-md-1' name='rcomp' id='checkcomp1' value='Compartir con usuarios de su empresa' onclick='javascript:MostrarCuadroEmpresa()' checked='checked' type='radio'>" +
                            "<label class='col-md-10 sin-padding-right'>Con usuarios de su Empresa </label>" +
                        "</div>" +
                        "<div  class='alinearConQuien sin-padding-right col-md-12'>" +
                            "<input class='col-md-1' name='rcomp' id='checkcomp2' value='Compartir con un email' onclick='javascript:MostrarCuadroEmail()' type='radio'>" +
                            "<label class='col-md-10 sin-padding-right'>Con un Email</label>" +
                        "</div>" +
                    "</div>" +
                    "<div id='checkboxes' class='reducirAlturaMitad sin-padding-right col-lg-12 col-md-12 col-sm-12 col-xs-12 '>" +
                        "<div class='reducirAlturaMitad col-md-12'>" +
                            "&nbsp;&nbsp;Obligada Lectura&nbsp;<input class='CheckLectura col-md-1' id='chklec' name='Checklectura' type='checkbox'>" +
                        "</div>" +
                    "</div>" +
                "</div>";

    $("#panelOpciones").append(html);
}

function añadirRellenablesFormularios() {
    var html = "<div id='rellenables' class='sin-padding-left col-lg-3 col-md-3 col-sm-4 col-xs-12'>" +
                 "<fieldset id='CuadroEmpresa' class='sin-padding-left sin-background col-md-12'>" +
                    "<label style='color:#333'>Datos: </label>" +
                     "<p class='unificar text-left sin-padding-left sin-padding-right col-md-12'>" +
                         "<label>Nombre empleado:</label>" +
                         "<input autocomplete='on' id='txtNewUser' class='textbox ui-autocomplete-input' type='text'/>" +
                     "</p>" +
                     "<p class='unificar text-left sin-padding-left sin-padding-right col-md-12 mitadAncho1'>" +
                         "<label>Visible desde:</label>" +
                         "<input id='txtVisDesde' class='textbox' type='text'/>" +
                     "</p>" +
                     "<p class='unificar text-left sin-padding-left sin-padding-right col-md-12 mitadAncho2'>" +
                         "<label>Visible hasta:</label>" +
                         "<input id='txtVisHasta' class='textbox' type='text'/>" +
                     "</p>" +
                 "</fieldset>" +
                 "<fieldset id='CuadroEmail' class='sin-padding-left sin-background col-md-12' style='display:none'>" +
                    "<p class='unificar text-left sin-padding-left sin-padding-right col-md-12'>" +
                         "<label>Introduzca un email:</label>" +
                         "<input id='txtNewEmail' class='textbox' type='text'/>" +
                    "</p>" +
                 "</fieldset>" +
                 "<fieldset id='CuadroComentario' class='sin-padding-left sin-background col-md-12' >" +
                    "<p class='unificar text-left sin-padding-left sin-padding-right col-md-12'>" +
                         "<label>Introduzca un comentario:</label>" +
                         "<textArea id='txtComentario' class='textAreaEmail' type='text'/>" +
                    "</p>" +
                 "</fieldset>" +
                 "<div id='botonesCompartirActualizar' class='col-md-12'>" +
                    "<button class='btn btn-AzulZertifika add-margin-bottom col-md-12' onclick='javascript:Añadirusuario();'>Añadir empleado a la lista</button>" +
                 "</div>" +
             "</div>";

    $("#panelOpciones").append(html);
}

function añadirListaCompartidosFormularios() {
    var html = "<div id='listaCompartidos' class=' col-lg-6 col-md-6 col-sm-4 col-xs-12'>" +
                    "<label style='color:#333'>Lista de usuarios compartidos: </label>" +
                    "<table id='tablaCompartidos'>" +
                        "<thead><td>Usuario compartido</td><td>Desde</td><td>Hasta</td><td title='Obligada Lectura'>O. L.</td><td>Elim.</td></thead>" +
                        "<tbody id='tbNewUsers'></tbody>" +
                    "</table>" +
               "</div>";

    $("#panelOpciones").append(html);
}


function VerAuditoriaFormularios(IdDoc, NomDoc) {

    $("#DivAuditoria").html("");
    $("#DivAuditoria").dialog({
        autoOpen: true,
        resizable: true,
        closeOnEscape: true,
        height: 550,
        width: 890,
        title: 'Auditoria del documento ' + NomDoc,
        modal: true,
        auto: false
    });
    cargarAuditoria(IdDoc);
    addBotonCerrarDialogo();
}

/**********************************************************************************************************************************/

function cargarFormularios(urlPath) {
    cargarArbolFormulariosControlador(urlPath, "jstree_demo_div_forms", "formularios");
    cargarArbolFormulariosControlador(urlPath, "jstree_demo_div_flujos", "flujos");
    cargarFormulariosServerControlador(urlPath, "contenido", "-1", false);
    cargarOpcionesFormulariosControlador(urlPath);
}

function getValueFromArray(aoData, Key) {

    for (i = 0; i < aoData.length; i++) {
        if (aoData[i].name == Key) {
            return aoData[i].value;
        }
    }
}

function devolverJSONControlador() {
    var jsonADataCreacionFlujo =
        '[' +
            '{' +
                '"sName": "Nombre",' +
                '"bSearchable": true,' +
                '"bSortable": true' +
            '},' +
            '{' +
                '"sName": "idFormulario",' +
                '"bSearchable": true,' +
                '"bSortable": true' +
            '}' +
        ']';

    return jsonADataCreacionFlujo;
}

function cargarFormulariosServerControlador(urlPath, divDestino, idTipoFormulario, esFlujo) {
    $("#" + divDestino).html("<table id='tablaFormularios'><thead><tr><th>Nombre</th><th>idFormulario</th></tr></thead><tbody></tbody></table>");
    tablaPendientes = $("#" + divDestino + " table").dataTable({
        stateSave: false,
        "language": {
            "url": "../index/json/spanish.json"
        },
        "paging": true,
        "processing": true,
        "searching": true,
        "bDestroy": true,
        "bServerSide": true,
        "sAjaxSource": urlPath + "GetFormulariosServer",
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Row click
            //$(nRow).on('click', function () {
            //    mostrarDocumento(aData[8], aData[9], aData[10]);
            //});
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            logsRequest = $.ajax({
                type: "POST",
                url: sSource,
                data: "{'sEcho': '" + getValueFromArray(aoData, "sEcho") + "', 'sSearch': '" + getValueFromArray(aoData, "sSearch")
                    + "', 'iDisplayLength': '" + getValueFromArray(aoData, "iDisplayLength") + "', 'iDisplayStart': '" + getValueFromArray(aoData, "iDisplayStart")
                    + "', 'iColumns': '" + getValueFromArray(aoData, "iColumns") + "', 'iSortingCols': '" + getValueFromArray(aoData, "iSortingCols")
                    + "', 'iSortCol': '" + getValueFromArray(aoData, "iSortCol_0") + "', 'sSortDir' : '" + getValueFromArray(aoData, "sSortDir_0")
                    + "', 'sColumns': '" + getValueFromArray(aoData, "sColumns") + "', 'idTipoFormulario': '" + idTipoFormulario + "', 'esFlujo': '" + esFlujo + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = jQuery.parseJSON(data.d);
                    fnCallback(json);
                    if (contains(window.location.href, "creacionFlujo2"))
                        marcarCheckboxFormularios(json);                    //cargarPopovers();
                }
            });
        },
        "aoColumns": JSON.parse(devolverJSONControlador())
    });
    $("#" + divDestino + " table").css("width", "100%");
}



function cargarArbolFormulariosControlador(urlPath, divTabla, tipoArbol) {
    var json = "{\"divDestino\" : \"\",\"tipoArbol\" : \"" + tipoArbol + "\"}";
    $.ajax({
        type: 'POST',
        url: urlPath + "MostrarArbolFormularios",
        data: json,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#" + divTabla).html("");
            $("#" + divTabla).html(data.d);
            $("#" + divTabla).jstree();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(XMLHttpRequest.responseText);
        }
    });
}

function cargarOpcionesFormulariosControlador(urlPath) {

    var select =
        "<select id='seleccionFormularios' class='col-lg-12 col-md-12 col-sm-12 col-xs-12' onchange='filtrarTiposFormulario(\"" + urlPath + "\")'>" +
            "<option value='usuario' >Formularios Usuario</option>" +
            "<option value='flujo' >Formularios Flujo</option>" +
        "</select>";
    $("#divTipoFormulario").html(select);
}

function filtrarTiposFormulario(urlPath) {

    var opcion = $("#seleccionFormularios").val();

    if (opcion == "usuario") {
        $("#jstree_demo_div_flujos").addClass("hidden");
        $("#jstree_demo_div_forms").removeClass("hidden");
        cargarFormulariosServerControlador(urlPath, "contenido", "-1", false);
    } else if (opcion == "flujo") {
        $("#jstree_demo_div_forms").addClass("hidden");
        $("#jstree_demo_div_flujos").removeClass("hidden");
        cargarFormulariosServerControlador(urlPath, "contenido", "-1", true);
    }
}