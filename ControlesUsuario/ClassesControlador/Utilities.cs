﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.Data;
using System.Net.Mail;
using System.IO;
using System.Web.Configuration;
using ControlesUsuario.ClassesControlador;

namespace ControlesUsuario.ClassesControlador
{
    public class Utilities
    {
        #region Generales
        public static String getParam(HttpContext c, String s)
        {
            string parametro = "";
            try
            {
                if (!string.IsNullOrEmpty(c.Request[s]))
                {
                    parametro = WebUtility.HtmlDecode(c.Request[s]);

                    if (s == "token" && parametro == "undefined")
                    {
                        if (c.Request.Cookies["token"] != null && c.Request.Cookies["token"].Value != null)
                        {
                            parametro = WebUtility.HtmlDecode(c.Request.Cookies["token"].Value.ToString());
                        }
                    }
                    else
                    {
                        //if (parametro == "undefined") parametro = "";


                        if (parametro != null && parametro.Contains("token=id="))
                        {
                            parametro = parametro.Replace("token=id=", "");
                        }

                        if (parametro != null && parametro.Contains("token="))
                        {
                            parametro = parametro.Replace("token=", "");
                        }

                        if (parametro != null && parametro.Contains("id="))
                        {
                            parametro = parametro.Replace("id=", "");
                        }
                    }
                    return tratarParam(parametro);
                }
                else
                {
                    // Si es nulo devuelvo vacío
                    return WebUtility.HtmlDecode(c.Request[s]);
                }
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getParam", "WorkFlow", -1, s + "." + parametro + "." + e.ToString(), e.InnerException + " . " + e.StackTrace + "");
                return "";
            }
        }

        public static string tratarParam(string s)
        {
            if (string.IsNullOrEmpty(s)) return null;
            s = WebUtility.HtmlDecode(s);
            if (s.Contains("--")) s = s.Replace("--", "");
            if (s.Contains("'")) s = s.Replace("'", "");
            if (s.Contains("\r\n"))
            {
                s = s.Replace("\r\n", Environment.NewLine);
            }
            else
            {
                if (s.Contains("\n")) s = s.Replace("\n", Environment.NewLine);
                else if (s.Contains("\r")) s = s.Replace("\r", Environment.NewLine);
            }
            
            if (s.Contains("\\")) s = s.Replace("\\", "");
            return s;
        }

        public static bool comprobarPermisosUsuario(string page, int idUsuario)
        {
            try
            {
                string query = string.Empty;
                DataTable dt = null;
                switch (page)
                {
                    case "Administracion":
                    case "CreacionFlujo":
                    case "Usuarios":
                        query = "SELECT TOP 1 idTipoUsuario FROM Usuarios WHERE idUsuario = '" + idUsuario + "' AND idTipoUsuario = 1";
                        // Queremos que sea el usuario y que sea administrador
                        dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (dt.Rows.Count == 1) return true;

                        query = "SELECT TOP 1 idUsuarioGrupo FROM UsuariosGrupos WHERE idUsuario = '" + idUsuario + "' AND AdministradorGrupo = 'True'";
                        // Queremos que sea el usuario y que sea administrador
                        dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (dt.Rows.Count == 1) return true;

                        query = "SELECT TOP 1 idUsuarioEmpresa FROM UsuarioEmpresa WHERE idUsuario = '" + idUsuario + "' AND Administrador = 'True'";
                        // Queremos que sea el usuario y que sea administrador
                        dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (dt.Rows.Count == 1) return true;
                        else return false;
    
                }
                return false;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("comprobarPermisosUsuario", "WorkFlow", idUsuario, page + "." + e.ToString(), e.InnerException + "");
                return false;
            }
        }

        public static string getDescripcionFlujo(int idFlujo, int idFicha)
        {
            string resultado = string.Empty;
            string query = string.Empty;
            try
            {
                query =
                "SELECT TOP 1 Descripcion FROM Descripcion WHERE idFlujo = " + idFlujo + " AND idFicha = " + idFicha;
                resultado = DatabaseConnection.executeScalarString(query, CommandType.Text, ConnectionString.WorkFlow);
                return resultado;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getDescripcionFlujo", "WorkFlow", -1, idFlujo + " . " + idFicha + "." + e.ToString(), e.InnerException + "");
                return resultado;
            }
        }

        //public static int enviarMail(int idUsuarioEmisor, string eMailDestino, string Asunto, string Body, bool tieneHtml )
        //{

        //    string output = null;

        //    try
        //    {
        //        string Host = "mail.zertifika.com";
        //        string Origen = "info@zertifika.com";
        //        string Pass = "r3dM#z41#z41#z41";
        //        string User = "info@zertifika.com";

        //            if (idUsuarioEmisor > 0)
        //            {

        //                MailMessage email = new MailMessage();
        //                email.To.Add(new MailAddress(eMailDestino));
        //                email.From = new MailAddress(Origen);
        //                email.Subject = Asunto;

        //                email.Body = Body;

        //                email.IsBodyHtml = tieneHtml;
        //                //email.Priority = MailPriority.Normal;

        //                string rutaFichero = string.Empty;

        //                int idEmpresaEmisor = BasicoFlujo.getIdEmpresa(idUsuarioEmisor);
        //                switch (idEmpresaEmisor)
        //                {
        //                    case 1:
        //                        rutaFichero = WebConfigurationManager.AppSettings["ruta"] + "Descargas/apps/EG/zflow.apk";
        //                        break;

        //                    default:
        //                        rutaFichero = WebConfigurationManager.AppSettings["ruta"] + "Descargas/apps/zflow107.apk";
        //                        break;


        //                }

                        

        //                if (File.Exists(rutaFichero))
        //                {
        //                    byte[] fichApk = File.ReadAllBytes(rutaFichero);
        //                    MemoryStream ms = new MemoryStream(fichApk);
        //                    email.Attachments.Add(new Attachment(ms, "ZFlow.apk", "application/vnd.android.package-archive"));
        //                }


        //                SmtpClient smtp = new SmtpClient();
        //                smtp.Host = Host;

        //                smtp.Credentials = new NetworkCredential(User, Pass);

        //                smtp.Send(email);
        //                email.Dispose();
        //                output = "Correo electrónico enviado satisfactoriamente.";
        //                return 1;
        //            }
                
        //        return -1;
        //    }
        //    catch (Exception e)
        //    {
        //        Trazas t = new Trazas("enviarMail", "WorkFlow", -1, idUsuarioEmisor + "." + output + "."
        //           + e.ToString(), e.InnerException + "");
        //        return -1;

        //    }

        
        //}


        public static bool comprobarAutorizacionUsuarioEstado(int idUsuario, int idEstado)
        {
            try
            {
                // OBTENEMOS EL USUARIO Y TIPO DE USUARIO QUE HA DE FIRMAR EL PASO
                string query = "SELECT TOP 1 idUsuarioAFirmar, TipoUsuario FROM Estados WHERE idEstado = '" + idEstado + "'";
                DataTable datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                // SINO EXISTE DEVOLVEMOS FALSO
                if (datos.Rows.Count < 1) return false;

                string TipoUsuario = datos.Rows[0]["TipoUsuario"].ToString();
                int idUsuarioAFirmar = Convert.ToInt32(datos.Rows[0]["idUsuarioAFirmar"]);
                // SI SE ESPERA QUE FIRME UN USUARIO
                switch (TipoUsuario)
                {
                    case "U":
                        // SI EL USUARIO A FIRMAR ES EL USUARIO QUE ESPERAMOS LE DAMOS PERMISO
                        if (idUsuario == idUsuarioAFirmar)
                        {
                            return true;
                        }else{
                            // Si no es puede que sea un usuario al que se le ha delegado la cuenta, lo comprobamos
                            query = "SELECT TOP 1 UsuarioDelegado FROM Usuarios WHERE idUsuario = " + 
                                idUsuarioAFirmar + " AND UsuarioDelegado = " + idUsuario;

                            DataTable dtDelegado = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                            if (dtDelegado.Rows.Count > 0)
                            {
                                // Es un usuario delegado
                                return true;
                            }
                            else
                            {
                                return false;
                            }

                        }

                    case "G":
                        // SI SE ESPERA QUE FIRME UN GRUPO
                        query = "SELECT TOP 1 idUsuarioGrupo FROM UsuariosGrupos WHERE idUsuario = '" + idUsuario + "' AND idGrupo = '" + idUsuarioAFirmar + "'";
                        datos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        // SI EL USUARIO FORMA PARTE DEL GRUPO A FIRMAR DEVOLVEMOS TRUE
                        if (datos.Rows.Count > 0) return true;
                        else return false;
                    case "C":
                        // ES UN CONDICIONAL, ES AUTOMÁTICO, DEJAMOS PASAR
                        return true;
                    default:
                        return false;
                }

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("comprobarAutorizacionUsuarioEstado", "WorkFlow", idUsuario, idEstado + "." + e.ToString(), e.InnerException + "");
                return false;
            }
        }


        public static string getDireccionFromUbicacion(string Latitud, string Longitud)
        {
            string direccion = "";
            string s = string.Empty;

            try
            {
                string url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + Latitud + "," + Longitud + "&sensor=true";

                using (WebClient client = new WebClient())
                {
                    s = client.DownloadString(url);

                    // Si hemos superado el ratio permitido no devolvemos la dirección
                    if (s.Contains("You have exceeded your rate-limit for this API.")) return "";

                    string[] stringSeparators = new string[] { "formatted_address" };

                    s = s.Split(stringSeparators, StringSplitOptions.None)[1];
                    stringSeparators[0] = "\n";
                    s = s.Split(stringSeparators, StringSplitOptions.None)[0];
                    s = s.Replace("\"", "");
                    s = s.Replace("\\", "");
                    s = s.Replace(":", "");
                    // Borro la coma final
                    s = s.Substring(0, s.Length - 1);

                    s = s.Replace("á", "a");
                    s = s.Replace("é", "e");
                    s = s.Replace("í", "i");
                    s = s.Replace("ó", "o");
                    s = s.Replace("ú", "u");

                    direccion = s;
                }

                return direccion;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("getDireccionFromUbicacion", "WorkFlow", -1, s + "." + Latitud + "." + Longitud +
                    "." + e.ToString(), e.InnerException + "");
                return "";
            }
        }

        public static string getNombreTablaFlujo(string idFlujo)
        {

                return "f" + "00000000".Substring(idFlujo.Length) + idFlujo;

        }

        public static bool comprobarAutorizacionUsuarioAdjunto(int idUsuario, int idAdjunto)
        {
            try
            {
                // OBTENEMOS EL DOCUMENTO DEL QUE CUELGA ESTE ADJUNTO
                string query = "SELECT TOP 1 idDocumento FROM DocumentosAdjuntos WHERE idDocAdjunto = '" + idAdjunto + "'";
                int idDoc = Convert.ToInt32(DatabaseConnection.executeScalar(query, CommandType.Text, ConnectionString.WorkFlow));

                // OBTENEMOS EL IDFLUJO E IDFICHA DE ESTE DOCUMENTO
                query = "SELECT TOP 1 idFlujo, idFicha FROM Documentos WHERE idDocumento = '" + idDoc + "'";
                DataTable docDatos = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                string idFlujo = docDatos.Rows[0]["idFlujo"].ToString();
                string idFicha = docDatos.Rows[0]["idFicha"].ToString();

                // OBTENEMOS QUIEN HA DE FIRMAR ESTE ESTADO
                query = "SELECT idUsuarioAFirmar, TipoUsuario FROM Estados WHERE idFlujo = '" + idFlujo + "' AND idFicha = '" + idFicha + "'";
                DataTable usuariosAFirmar = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                foreach (DataRow dr in usuariosAFirmar.Rows)
                {
                    string tipoUsuario = dr["TipoUsuario"].ToString();
                    int idUsuarioAFirmar = Convert.ToInt32(dr["idUsuarioAFirmar"]);

                    if (tipoUsuario == "G")
                    {
                        // SI ES GRUPO MIRAMOS SI ESE USUARIO FORMA PARTE DEL GRUPO
                        query = "SELECT TOP 1 idUsuarioGrupo FROM UsuariosGrupos WHERE idGrupo = '" + idUsuarioAFirmar + "' AND idUsuario = '" + idUsuario + "'";
                        DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (dt.Rows.Count > 0) return true;

                    }
                    else
                    {
                        //SI ES USUARIO MIRAMOS QUE SEA EL MISMO
                        if (idUsuarioAFirmar == idUsuario) return true;

                    }
                }
                return false;


            }
            catch (Exception e)
            {
                Trazas t = new Trazas("comprobarAutorizacionUsuarioAdjunto", "WorkFlow", idUsuario, idAdjunto +
                    "." + e.ToString(), e.InnerException + "");
                return false;
            }
        }


        /*
        public static void addEvento(string Titulo, string Texto, string idUsuario)
        {
            try
            {
                string query = "INSERT INTO Eventos (Titulo, Texto, idUsuario) VALUES ('" + Titulo + "','" + Texto + "','" + idUsuario + "')";
                DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
            }
            catch (Exception e)
            {

            }
        }
        */

        public static void addEvento(string Titulo, string Texto, string idUsuario, string idDoc = null, string idEstado= null)
        {
            try
            {
                string query = "INSERT INTO Eventos (Titulo, Texto, idUsuario, idDoc, idEstado) VALUES ('" + Titulo + "','" + Texto + "','" + idUsuario + "','" + idDoc + "','" + idEstado + "')";
                DatabaseConnection.executeNonQueryInt(query, CommandType.Text, ConnectionString.WorkFlow);
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("addEvento", "WorkFlow", -1, Titulo +
                    "." + Texto + "." + idUsuario + "." + idDoc + "." + idEstado + "." + e.ToString(), e.InnerException + "");
            }
        }

        #endregion

        #region Seguridad y criptografía
        public static String GetSHA1(String data)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(data, "SHA1");
        }


        public static string GetSHA512WithSalt(string data)
        {
            try{

                data = "pr3-" + data + "-p0st";

                byte[] hash = Encoding.UTF8.GetBytes(data);

                using (SHA512 shaM = new SHA512Managed())
                {
                    hash = shaM.ComputeHash(hash);
                }
                return Convert.ToBase64String(hash);
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("GetSHA512", "WorkFlow", -1, e.ToString() + ". " + data, e.InnerException + "");
                return "";
            }
        }

        public static string GetSHA512(string data)
        {
            try
            {

                byte[] hash = Encoding.UTF8.GetBytes(data);

                using (SHA512 shaM = new SHA512Managed())
                {
                    hash = shaM.ComputeHash(hash);
                }
                return Convert.ToBase64String(hash);
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("GetSHA512", "WorkFlow", -1, e.ToString() + ". " + data, e.InnerException + "");
                return "";
            }
        }

        public static int comprobarValidezUsuario(string tokenUsuario)
        {
            string query = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(tokenUsuario) || tokenUsuario == "undefined" || !tokenUsuario.Contains('I'))
                    return -1;

                if (tokenUsuario.Contains(';')) tokenUsuario = tokenUsuario.Split(';')[0];

                if (tokenUsuario.Contains("token=id="))  tokenUsuario = tokenUsuario.Replace("token=id=", "");
                if (tokenUsuario.Contains("id=")) tokenUsuario = tokenUsuario.Replace("id=", "");
                
                if (tokenUsuario.Contains('I'))
                {
                    string idUsuario = tokenUsuario.Split('I')[0];

                    string passToken = tokenUsuario.Replace(tokenUsuario.Split('I')[0] + "I", "");
                    if (Convert.ToInt32(idUsuario) > 0)
                    {
                        query = "SELECT TOP 1 idUsuario FROM usuarios WHERE (PasswordE = '" + passToken + "' OR PasswordE2 = '" + passToken + "') AND idUsuario = " + idUsuario + " AND Activo = 'S'";
                        DataTable dt = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                        if (dt.Rows.Count > 0)
                        {
                            return 1;
                        }
                    }
                }
                return -1;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("comprobarValidezUsuario", "WorkFlow", -1, e.ToString() + tokenUsuario, e.InnerException + "");
                return -1;
            }
        }
        #endregion


        public static bool validezUsuario(HttpRequest hr)
        {
            try
            {

                if ((hr.Cookies["token"] != null) &&
                    !string.IsNullOrEmpty(hr.Cookies["token"].Value) &&
                    hr.Cookies["token"].Value.Contains('I'))
                {
                    if (hr.Cookies["token"].Value.Contains('='))
                    {
                        string tokenUsuario = Utilities.tratarParam(hr.Cookies["token"].Value.Split('=')[1]);
                        if (Utilities.comprobarValidezUsuario(tokenUsuario) == 1)
                        {
                            int idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);
                            if (idUsuario > 0) return true;
                        }
                    }
                    else
                    {
                        string tokenUsuario = Utilities.tratarParam(hr.Cookies["token"].Value);
                        if (Utilities.comprobarValidezUsuario(tokenUsuario) == 1)
                        {
                            int idUsuario = Convert.ToInt32(tokenUsuario.Split('I')[0]);
                            if (idUsuario > 0) return true;
                        }
                    }
                }

                return false;

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("validezUsuario", "WorkFlow", -1, e.ToString(), e.InnerException + "");
                return false;
            }
        }

        public static string jsonToCampos(string jsonTexto, string campoAdicional = "")
        {
            string resp = string.Empty;
            try
            {

                resp = "<campos><campo>";
                // quitamos llaves y dividimos en campos
                string[] camposJSON = jsonTexto.Replace("{", "").Replace("}", "").Split(',');
                for (int i = 0; i < camposJSON.Length; i++)
                {
                    string nombreCampo = camposJSON[i].Split(':')[0];
                    nombreCampo = nombreCampo.Replace("\"", "").Trim();
                    string valorCampo = camposJSON[i].Split(':')[1];
                    valorCampo = valorCampo.Replace("\"", "").Trim();

                    resp += "<" + nombreCampo + ">" + valorCampo + "</" + nombreCampo + ">";
                }

                resp += campoAdicional;

                resp += "</campo></campos>";



                return resp;
            }
            catch (Exception e)
            {
                Trazas t = new Trazas("jsonToCampos", "WorkFlow", -1, e.ToString(), e.InnerException + "");
                return "-1";
            }
        }

        public static int comprobarValidezInvitacion(string tokenInvitacion, string email)
        {
            try
            {
                //Comprobamos si existe esa invitacion y si iba a ese mail
                string query = "SELECT TOP 1 idInvitacion FROM Invitaciones WHERE token = '" + tokenInvitacion + "' AND mailReceptor = '" + email + "'";
                DataTable dtInvitacion = DatabaseConnection.executeNonQueryDT(query, CommandType.Text, ConnectionString.WorkFlow);
                if (dtInvitacion.Rows.Count > 0)
                {
                    int idInvitacion = Convert.ToInt32(dtInvitacion.Rows[0]["idInvitacion"]);
                    return idInvitacion;
                }
                else
                {
                    return -1;
                }

            }
            catch (Exception e)
            {
                Trazas t = new Trazas("comprobarValidezInvitacion", "WorkFlow", -1, e.ToString() + tokenInvitacion, e.InnerException + "");
                return -1;
            }
        }
    }
}